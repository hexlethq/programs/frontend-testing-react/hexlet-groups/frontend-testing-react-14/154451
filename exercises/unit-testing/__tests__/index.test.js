describe('test assign function', () => {
  test('main', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toEqual({ k: 'v', b: 'b', a: 'a' });
  });

  test('target object returning', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toBe(target);
  });

  test('correct properties overriding', () => {
    const target = { k: 'v', b: 'b' };
    const src = { k: 'v2' };
    const src2 = { k: 'v3' };

    const result = Object.assign(target, src, src2);

    expect(result).toEqual({ k: 'v3', b: 'b' });
  });

  test('cloning object', () => {
    const target = {};
    const src = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toEqual({ k: 'v2', a: 'a' });
  });

  test('pass non object', () => {
    const array = [1, 2];
    const str = 's';
    const num = 3;
    const target = {};
    const resultArray = Object.assign(target, array, str, num);

    expect(resultArray).toEqual({ 0: 's', 1: 2 });
  });

  test('assing to primitive', () => {
    const src = { k: 'v2', a: 'a' };
    const result = Object.assign(2, src);

    expect(result).toMatchObject({ k: 'v2', a: 'a' });
  });

  test('should frow error', () => {
    const src = { k: 'v2', a: 'a' };

    expect(() => Object.assign(null, src)).toThrow();
  });

  test('override ridable property', () => {
    const readable = Object.defineProperty({}, 'prop', {
      value: 1,
      writable: false,
    });
    const src = { prop: 'test' };

    expect(() => Object.assign(readable, src)).toThrow();
  });
});
