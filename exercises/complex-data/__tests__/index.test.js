const faker = require('faker');

// BEGIN
describe('test createTransaction', () => {
  test('entries has right type', () => {
    expect(faker.helpers.createTransaction()).toMatchObject({
      amount: expect.stringMatching(/^\d(\.\d)*/),
      date: expect.any(Date),
      account: expect.stringMatching(/\d/),
      name: expect.any(String),
      type: expect.any(String),
    });
  });

  test('account length', () => {
    const trans = faker.helpers.createTransaction();
    expect(trans.account.length).toBe(8);
  });

  test('amount limits', () => {
    const trans = faker.helpers.createTransaction();
    expect(Number(trans.amount)).toBeGreaterThanOrEqual(0);
    expect(Number(trans.amount)).toBeLessThanOrEqual(1000);
  });

  test('generate unic transactions', () => {
    expect(faker.helpers.createTransaction()).not.toBe(faker.helpers.createTransaction());
  });
});
// END
