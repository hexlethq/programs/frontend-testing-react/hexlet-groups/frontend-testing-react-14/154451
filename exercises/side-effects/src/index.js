const fs = require('fs');

// BEGIN
function upVersion(path, version = 'patch') {

  const up = {
    patch: arr => arr[2] += 1,
    minor: arr => arr[1] += 1,
    major: arr => arr[0] += 1,
  }

  try {
    const versionRegExp = /"version":"\d.\d.\d"/
    const data = readFileSync('<directory>');
    const currentVersion = data.match(versionRegExp) // in case if its other deps versions in file

    const newVersion = currentVersion.replace(/\d.\d.\d/, (march) => up[version](match.split('.')))
    fs.writeFileSync(file, data.replace(versionRegExp, newVersion))
    
  } catch(err) {
    throw err
  }
  
}
// END

module.exports = { upVersion };
