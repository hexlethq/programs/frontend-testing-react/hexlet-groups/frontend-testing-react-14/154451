const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('array sort function', () => {
  it('array is sorted', () => {
    fc.assert(fc.property(fc.array(fc.integer()), (arr) => {
      expect(sort(arr)).toBeSorted();
    }));
  });

  it('sort is idempotent function', () => {
    fc.assert(fc.property(fc.array(fc.integer()), (arr) => {
      expect(sort(arr)).toEqual(sort(sort(arr)));
    }));
  });

  it('descending sorting', () => {
    fc.assert(fc.property(fc.array(fc.integer()), (arr) => {
      expect(sort(arr).reverse()).toBeSorted({ descending: true });
    }));
  });
});
// END
